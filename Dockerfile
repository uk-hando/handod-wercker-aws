FROM python:3.6.2

MAINTAINER london-digital@hogarth-ogilvy.com

RUN apt-get update
RUN apt-get install -y git mercurial zip

RUN pip install awscli boto3
